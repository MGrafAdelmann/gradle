package com.bongladesch.sebidaMaven.accounts;

import java.util.Date;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Represents all data of a customer.
 * @author ken
 */
public class Customer
{
	//Initialisierung eines Loggers
	private static Logger logger = LogManager.getLogger(Customer.class);
	
	// User Data
	private String firstName;
	private String lastName;
	private Date birthday;
	private Address address;
	
	/* Constructor */
	public Customer(String fn, String ln, Date bd, Address address)
	{
		setFirstName(fn);
		setLastName(ln);
		setBirthday(bd);
		setAddress(address);
		logger.info("Kunde " + firstName + " " + lastName + " wurde erstellt");
		logger.debug("Kundenname: " + lastName);
	}
	
	/* Getter and Setter */
	
	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public Date getBirthday()
	{
		return birthday;
	}

	public void setBirthday(Date birthday)
	{
		this.birthday = birthday;
	}

	public Address getAddress()
	{
		return address;
	}

	public void setAddress(Address address)
	{
		this.address = address;
	}
	
	@Override
	public String toString()
	{
		String fn = "\tVorname: " + getFirstName();
		String ln = "\n\tNachname: " + getLastName();
		String bd = "\n\tGeburtstag: " + getBirthday();
		String ad = "\n\tAdresse:\n" + getAddress();
		return fn + ln + bd + ad;
	}
}
