package com.bongladesch.sebidaMaven.other;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class ArraysS 
{
	private static Logger logger = LogManager.getLogger(ArraysS.class);
	public void print(int [][] temp)
	{
		for(int j = 0; j < temp.length; j++)
		{
			for(int i = 0; i < temp[j].length; i++)
			{
				System.out.print(temp[j][i] + " ");
			}
			System.out.print("\n");
		}
	}
	public int[][] pascal(int n)
	{
		int [][] temp = new int [n][];
		temp[0] = new int [] { 1 };
		for(int i = 1; i < n; i++)
		{
			temp[i] = new int [i+1];
			temp[i][0] = temp[i][i] = 1;
			for(int j = 1; j < i; j++)
			{
				temp[i][j] = temp[i-1][j-1] + temp[i-1][j]; 
			}
		}
		return temp;
	}
	public boolean isPalindrome (String s)
	{
		if(s == null)
			return false;
		int n = s.length();
		for(int i = 0; i < n/2; i++)
		{
			if(s.charAt(i) != s.charAt(n - i - 1))
				return false;
		}
		return true;
		
	}
	public String toLower(String s)
	{
		String r = "";
		for(int i = 0; i < s.length(); i++)
		{
			char b = s.charAt(i);
			if('A' <= b && 'Z' >= b)
				b += 'a' - 'A';
			r += b;
		}
		return r;
		
		
	}
	/*
	public static void main(String[] args) 
	{
		Arrays a = new Arrays();
		a.print(a.pascal(2));
		a.print(a.pascal(7));
		
		
		System.out.println(a.isPalindrome("anna"));
		System.out.println(a.isPalindrome("neda"));
		
		System.out.println(a.toLower("ANNA"));
		System.out.println(a.toLower("{anna"));
		int m = 2,b = 1;
		int n = m < b ? m : b;
		System.out.println(n);
		m = 1;b = 1;
		n = m < b ? m : b;
		System.out.println(n);
		m = 3;b = 4;
		n = m < b ? m : b;
		System.out.println(n);
	}
	*/
}
