package com.bongladesch.sebidaMaven.other;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Sortieren1 
{
	//Initialisierung eines Loggers
	private static Logger logger = LogManager.getLogger(Sortieren1.class);
	//public static void main (String [] args) 
	//{
		//int [] A = {2,5,6,7,9,3,2,10,7,1};
		//Ausgabe(A);
		//A = insertionsort4(A);
		//Ausgabe(A);
	//}
	public static void insertionsort (int[]A)
	{
		
		int x, j;
		for(int i = 1; i < A.length; i++)
		{
			x = A[i];
			j = i;
			while((j > 0) && (A[j - 1] > x))
			{
				A[j] = A[j-1];
				j = j - 1;
			}
			A[j] = x;
		}
	}
	public static void insertionsort2 (int[]A)
	{
		
		int x, j;
		for(int i = A.length - 2; i >= 0; i--)
		{
			x = A[i];
			j = i;
			while((j < A.length - 1) && (A[j + 1] < x))
			{
				A[j] = A[j+1];
				j = j + 1;
			}
			A[j] = x;
		}
	}
	public static void insertionsort3 (int[]A)
	{
		
		int x, j;
		for(int i = 1; i < A.length; i++)
		{
			x = A[i];
			j = i;
			while((j > 0) && (A[j - 1] < x))
			{
				A[j] = A[j-1];
				j = j - 1;
			}
			A[j] = x;
		}
	}
	public static int[] insertionsort4 (int[]A)
	{
		int [] a;
		int j = 1;
		insertionsort(A);
		for(int i = 0; i < A.length - 1; i++)
		{
			if(A[i] != A[i+1])
			{
				j++;
			}
		}
		
		a = new int[j];
		
		j = 1;
		
		a[0] = A[0];
		
		for(int i = 1; i < A.length; i++)
		{
			if(A[i] != A[i - 1])
			{
				
				a[j] = A[i];
				j++;
			}
		}
		return a;
	}
	public static void Ausgabe(int[] A)
	{
		for(int i = 0; i < A.length; i++)
		{
			System.out.println(A[i]);
		}
	}
}
