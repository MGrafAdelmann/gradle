package com.bongladesch.sebidaMaven.other.math;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Const extends Expr
{
	private static Logger logger = LogManager.getLogger(Const.class);
	private double value;
	
	public Const(double v)
	{
		value = v;
	}
	public double compute()
	{
		return value;
	}
	public String toString()
	{
		return value + "";
	}
	public boolean equals (Object other) 
	{
		if (!(other instanceof Const)) return false;
		Const that = (Const)other;
		return this.value == that.value;
	}
}
