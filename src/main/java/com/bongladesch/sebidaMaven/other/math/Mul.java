package com.bongladesch.sebidaMaven.other.math;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Mul extends Bin
{
		private static Logger logger = LogManager.getLogger(Mul.class);
	public Mul(Expr left, Expr right)
	{
		super(left,right);
	}
	public double combine(double l, double r)
	{
		return l * r;
	}
	public String oper()
	{
		return "*";
	}
}
