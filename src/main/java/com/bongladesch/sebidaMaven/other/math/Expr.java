package com.bongladesch.sebidaMaven.other.math;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public abstract class Expr 
{
	private static Logger logger = LogManager.getLogger(Expr.class);
	public abstract double compute();
	public boolean equals(String s)
	{
		return this.toString().equals(s);
	}
}
