package com.bongladesch.sebidaMaven.other.math;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Sub extends Bin
{
	private static Logger logger = LogManager.getLogger(Sub.class);
	public Sub(Expr left, Expr right)
	{
		super(left,right);
	}
	public double combine(double l, double r)
	{
		return l - r;
	}
	public String oper()
	{
		return "-";
	}
}
