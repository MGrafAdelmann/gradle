package com.bongladesch.sebidaMaven.other.math;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public abstract class Bin extends Expr
{
	private static Logger logger = LogManager.getLogger(Bin.class);
	private Expr left;
	private Expr right;
	protected Bin (Expr l, Expr r) 
	{ 
		left = l; 
		right = r; 
	}
	protected abstract double combine (double l, double r);
	
	protected abstract String oper ();
	
	public double compute () 
	{
		return combine(left.compute(), right.compute());
	}
	public String toString () 
	{
		return "(" + left + oper() + right + ")";
	}
	
	public boolean equals (Object other) 
	{
		if (!(other instanceof Bin)) return false;
		Bin that = (Bin)other;
		return this.oper().equals(that.oper())
				&& this.left.equals(that.left)
				&& this.right.equals(that.right);
	}

}
