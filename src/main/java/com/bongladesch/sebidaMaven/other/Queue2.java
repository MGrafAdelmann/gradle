package com.bongladesch.sebidaMaven.other;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Queue2 
{
	private static Logger logger = LogManager.getLogger(Queue2.class);
	/*public static void main(String[] args) 
	{
		char [] Q = new char[9];
		Queue2 test = new Queue2();
		System.out.println(test.dequeue(Q));
		test.enqueue(Q, 'H');
		test.enqueue(Q, 'A');
		test.enqueue(Q, 'L');
		test.enqueue(Q, 'L');
		test.enqueue(Q, 'O');
		test.enqueue(Q, 'W');
		test.enqueue(Q, 'E');
		test.enqueue(Q, 'E');
		test.enqueue(Q, 'N');
		test.enqueue(Q, 'N');
		System.out.println(test.anzahl());
		System.out.println(test.dequeue(Q));
		System.out.println(test.dequeue(Q));
		System.out.println(test.dequeue(Q));
		System.out.println(test.dequeue(Q));
		System.out.println(test.dequeue(Q));
		System.out.println(test.dequeue(Q));
		System.out.println(test.dequeue(Q));
		System.out.println(test.dequeue(Q));
		System.out.println(test.dequeue(Q));
		System.out.println(test.anzahl());
		System.out.println(test.dequeue(Q));
	}
	*/
	private int length = 0, head = 0,tail = 0;
	public boolean isEmpty(char [] Q)
	{
		if(head == tail)
			return true;
		else
			return false;
	}
	public void enqueue(char [] Q ,char x)
	{
		if(tail >= 0 && tail < Q.length)
		{
			Q[tail] = x;
			if(tail == length)
				tail = 1;
			else 
				tail = tail + 1;
		}
		else
		{
			System.out.println("Fehler in Prozedur enqueue. Es können keine Elemente mehr hinzugefügt werden");
		}
	}
	public char dequeue(char [] Q)
	{
		if(!isEmpty(Q))
		//if(head >= 0 && head < Q.length)
		{
			char x = Q[head];
			
			if(head == length)
				head = 1;
			else
				head = head + 1;
			return x;
		}
		else
		{
			System.out.println("Fehler in Funktion dequeue. Es können keine Elemente mehr ausgelesen werden");
			return ' ';
		}
	}
	public int anzahl()
	{
		return tail - head;
	}
}
