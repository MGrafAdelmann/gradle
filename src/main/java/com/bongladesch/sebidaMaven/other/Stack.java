package com.bongladesch.sebidaMaven.other;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Stack 
{
	private static Logger logger = LogManager.getLogger(Stack.class);
	/*
	public static void main(String[] args) 
	{
		int [] S = new int[5];
		Stack test = new Stack();
		test.push(S, 3);
		test.push(S, 4);
		test.push(S, 5);
		test.push(S, 6);
		test.push(S, 7);
		System.out.println(test.pop(S));
		System.out.println(test.pop(S));
		System.out.println(test.pop(S));
		System.out.println(test.pop(S));
		System.out.println(test.pop(S));
	}
	*/
	private int top = 0;

	public boolean isEmprty(int [] S)
	{
		if(S[top] == 0)
			return true;
		else 
			return false;
	}
	public void push(int [] S ,int x)
	{
		if(top < S.length - 1)
		{
			top = top + 1;
			S[top] = x;
		}
		else
			System.out.println("Overflow");
	}
	public String pop(int [] S)
	{
		if(isEmprty(S))
		{
			return "Underflow";
		}
		else
		{
			top = top - 1;
			return String.valueOf(S[top + 1]);
		}
	}
}
