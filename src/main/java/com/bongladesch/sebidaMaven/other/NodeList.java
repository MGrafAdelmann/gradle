package com.bongladesch.sebidaMaven.other;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class NodeList 
{
	private static Logger logger = LogManager.getLogger(NodeList.class);
	private ElemtList head,tail;
	public NodeList()
	{
		head = tail = null;
	}
	public ElemtList listSearch(NodeList L, int k)
	{
		ElemtList x = L.head;
		while(x != null && x.key != k)
			x = x.next;
		return x;
	}
	public void listInsert(NodeList L, ElemtList x)
	{
		x.next = L.head;
		x.prev = null;
		if(L.head != null)
			L.head.prev = x;
		else
			tail = x;
		L.head = x;
	}
	public void listInsert(NodeList L, int x)
	{
		ElemtList y = new ElemtList(x);
		listInsert(L, y);
	}
	public void listInsertSort(NodeList L, ElemtList x)
	{
		ElemtList y = L.listSearch(L, x.key);
		
	}
	public void listInsertSort(NodeList L, int x)
	{
		ElemtList y = new ElemtList(x);
		listInsertSort(L, y);
	}
	public void listDelete(NodeList L, ElemtList x)
	{
		if(x.prev != null)
			x.prev.next = x.next;
		else
			L.head = x.next;
		if(x.next != null)
			x.next.prev = x.prev;
	}
	public void listPrint(NodeList L)
	{
		if(L.head != null)
		{
			ElemtList x = L.head;
			System.out.println(x.key);
			while(x.next != null)
			{
				x = x.next;
				System.out.println(x.key);
			};
		}
	}
	/*
	public static void main(String [] args)
	{
		NodeList liste = new NodeList();
		liste.listInsert(liste, 1);
		liste.listInsert(liste, 2);
		ElemtList x = new ElemtList(3);
		liste.listInsert(liste, x);
		liste.listInsert(liste, 4);
		liste.listInsert(liste, 5);
		liste.listPrint(liste);
		liste.listDelete(liste, x);
		liste.listDelete(liste, liste.listSearch(liste, 2));
		liste.listPrint(liste);
	}
	*/
}