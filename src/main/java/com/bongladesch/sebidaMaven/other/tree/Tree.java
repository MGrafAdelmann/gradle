package com.bongladesch.sebidaMaven.other.tree;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Repraesentation eines Binaeren Suchbaums
 * Hinweis: Im Gegensatz zum Pseudocode der Vorlesungsunterlagen fehlt 
 * bei einige Methoden das erste Argument (root, etc.), 
 * da in dieser Klasse die Wurzel als Klassenvariable deklariert wurde. 
 */

/**
 * @author clecon
 *
 */
public class Tree {

	private static Logger logger = LogManager.getLogger(Tree.class);

	public Node root;
	
	
	/**
	 * Erstellt einen leeren Binaeren Suchbaum
	 */
	public Tree() {
		root = null;
	} // 

	/**
	 * Erstellt einen neuen Binaren Suchbaum mit einem Wurzeknoten
	 * @param key Wert des Wurzelknontens
	 */
	public Tree(int key) {
		root = new Node(key);
	}

	public void inorderTreeWalk(Node x) 
	{
		if (x != null)
		{
			inorderTreeWalk(x.left);
			System.out.printf("%d",x.key);
			System.out.printf("\n");
			inorderTreeWalk(x.right);
		}
		// To do
	} // inrderTreeWald
	
	public void treeInsert(Node z)
	{
		z.right = null;
		z.left = null;
		z.parent = null;
		Node y = null;
		if (root == null)
		{
			root = z;
		}
		else
		{
			Node x = root;
			while(x != null)
			{
				y = x; // möglicher Elternknoten
				if(z.key < y.key)
					x = x.left;
				else 
					x = x.right;
			}
			z.parent = y;
			if(z.key < y.key)
				y.left = z;
			else
				y.right = z;
		}
	} // treeInsert
	
	public void treeDelete(Node delNode) 
	{
		Node x,z = delNode;
		if(z.left == null || z.right == null)
			delNode = z;
		else
			delNode = treeSuccessor(z);
		if(delNode.left != null) 
			x = delNode.left;
		else 
			x = delNode.right;
		// x ist Kind von delNode
		if(x != null)
			x.parent = delNode.parent;
		// Kind von delNode bekommt neuen Elternknoten
		if(delNode.parent == null)// falls delNode keinen Elternknoten hat,
			// hat auch x keinen
			root = x;
		else
		{
			if(delNode == delNode.parent.left)
				delNode.parent.left = x;
			else
				delNode.parent.right = x;
		}
		if(z != null)
		{
			z.key = delNode.key;
		//Satellitendaten(z) = Satellitendaten(delNode)
		// ggf. Aufräumarbeiten, bspw. left(z) = NIL
		}
		// To do
	} // treeDelete
	
	public Node treeSearch(Node x, int k) 
	{
		if(x == null || k == x.key)
			return x;
		if(k < x.key)
			return treeSearch(x.left,k);
		else
			return treeSearch(x.right,k);
	} // treeSearch
	
	public Node treeMinimum(Node x) 
	{
		if(x == null) 
			return null;
		while(x.left != null)
			x = x.left;
		return x;
	} // treeMinimum
	
	public Node treeSuccessor(Node x) 
	{
		if(x == null)
			return null;
		if(x.right != null)
			return treeMinimum(x.right);
		Node y = x.parent;
		while(y != null && x == y.right)
		{
			x = y;
			y = y.parent;
		}
		return y;
	} // treeSuccessor
	/**
	 * main-Methode
	 * @param args
	 */
	 /*
	public static void main(String[] args) {
		/*
		Tree tree = new Tree();
		tree.treeInsert(new Node(1));
		tree.treeInsert(new Node(2));
		tree.treeInsert(new Node(5));
		tree.treeInsert(new Node(9));
		tree.treeInsert(new Node(3));
		Node knoten = new Node(4);
		tree.treeInsert(knoten);
		System.out.println("Ausgabe der Knoten des Baumes in aufsteigender Reihenfolge:");
		tree.inorderTreeWalk(tree.root);
		tree.treeDelete(tree.treeSearch(tree.root, 4));
		System.out.println("Ausgabe der Knoten des Baumes in aufsteigender Reihenfolge:");
		tree.inorderTreeWalk(tree.root);
		//tree.preorderTreeWalk(tree.root);
		//tree.postorderTreeWalk(tree.root);
		 * 
		 */
		/*
		SyntaxNode test = new SyntaxNode();
		test = test.init(test);
		test.rechnen(test);
		*/
		

	//} 
	public void preorderTreeWalk(Node x)
	{
		if(x != null)
		{
			System.out.printf("%d",x.key);
			preorderTreeWalk(x.left);
			preorderTreeWalk(x.right);
		}
	}
	public void postorderTreeWalk(Node x)
	{
		if(x != null)
		{
			postorderTreeWalk(x.left);
			postorderTreeWalk(x.right);
			System.out.printf("%d",x.key);
			System.out.printf("\n");
		}
	}
	public Node treeSearchIterativ(Node x, int k) 
	{
		while(x != null && k != x.key)
		{
			if(k < x.key)
				x = x.left;
			else
				x = x.right;
		}
		return x;
	} // treeSearchIterativ
	
	public Node treeMaximum(Node x) 
	{
		if(x == null) 
			return null;
		while(x.right != null)
			x = x.right;
		return x;
	} // treeMaximum

	/*
	public Node treePredeccessor(Node x) 
	{
		if(x == null)
			return null;
		if(x.left != null)
			return treeMaximum(x.right);
		Node y = parent(x);
		while(y != null && x == y.left)
		{
			x = y;
			y = parent(y);
		}
		return y;
	} // treePredeccessor
	*/

} // class Tree
