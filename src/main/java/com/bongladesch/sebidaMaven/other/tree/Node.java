package com.bongladesch.sebidaMaven.other.tree;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
/**
 * Knoten eines Binaren Suchbaums
 */

/**
 * @author clecon
 *
 */
public class Node {
	private static Logger logger = LogManager.getLogger(Node.class);

	int key;
	Node left;
	Node right;
	Node parent;

	public Node(int key) {
		this.key = key;
		left = null;
		right = null;
		parent = null;
	} // Konstruktor
	
	
} // class Node
