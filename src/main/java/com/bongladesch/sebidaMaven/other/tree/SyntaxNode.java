package com.bongladesch.sebidaMaven.other.tree;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class SyntaxNode 
{
	private static Logger logger = LogManager.getLogger(SyntaxNode.class);
	int key;
	char cKey;
	SyntaxNode left;
	SyntaxNode right;
	SyntaxNode parent;
	public SyntaxNode()
	{}
	public SyntaxNode(char cKey)
	{
		this.cKey = cKey;
		left = null;
		right = null;
		parent = null;
	}
	public SyntaxNode(int key)
	{ 
		this.key = key;
		cKey = ' ';
		left = null;
		right = null;
		parent = null;
	}
	public SyntaxNode init(SyntaxNode root) 
	{
		root = new SyntaxNode('x');
		SyntaxNode nPlus = new SyntaxNode('+');
		SyntaxNode nMinus = new SyntaxNode('-');
		SyntaxNode nDivide = new SyntaxNode('/');
		SyntaxNode n4 = new SyntaxNode(4);
		SyntaxNode n9 = new SyntaxNode(9);
		SyntaxNode n7 = new SyntaxNode(7);
		SyntaxNode n9_ = new SyntaxNode(9);
		SyntaxNode n3 = new SyntaxNode(3);
		
		root.left = nPlus;
		root.right = nMinus;
		nPlus.left = nDivide;
		nPlus.right = n4;
		nMinus.left = n9;
		nMinus.right = n7;
		nDivide.left = n9_;
		nDivide.right = n3;
		
		return root;
		
	} // init
	public SyntaxNode rechnen(SyntaxNode root)
	{
		System.out.println(lastSyntaxNodeOper(root));
		return root;
	}
	public int lastSyntaxNodeOper(SyntaxNode oper)
	{
		int term = 0, term1 = 0, term2 = 0;
		if(oper.left.cKey != ' ')
		{
			term1 = lastSyntaxNodeOper(oper.left);
		}
		if(oper.right.cKey != ' ')
		{
			term2 = lastSyntaxNodeOper(oper.right);
		}
		if(oper.left.cKey == ' ')
			term1 = oper.left.key;
		if(oper.right.cKey == ' ')
			term2 = oper.right.key;
		switch(oper.cKey)
		{
			case 'x':
				term = term1 * term2;
				break;
			case '/':
				term = term1 / term2;
				break;
			case '+':
				term = term1 + term2;
				break;
			case '-':
				term = term1 - term2;
				break;
		}
		return term;
		
	}
	/*
	public static void main(String[] args)
	{
		SyntaxNode test = new SyntaxNode();
		test = test.init(test);
		test.rechnen(test);
	} // main
	*/

}
