package com.bongladesch.sebidaMaven.other.tree;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class StringNode 
{
	private static Logger logger = LogManager.getLogger(StringNode.class);

	String wort;
	StringNode left;
	StringNode right;
	StringNode parent;
	public StringNode(String wort) 
	{
		this.wort = wort;
		left = null;
		right = null;
		parent = null;
	} // Konstruktor
}
