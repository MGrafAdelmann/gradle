package com.bongladesch.sebidaMaven.other.tree;

import java.util.Arrays; 
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class StringTree 
{
	private static Logger logger = LogManager.getLogger(StringTree.class);
	public static StringNode root;
	public double anzahl;
	public int anzahlAll;
	private static String[] words;
	public StringTree()
	{
		root = null;
		anzahl = 0;
	}
	public void haeufigkeit(StringNode root, String wort)
	{
		anzahl = 0;
		haeufigkeitIntern(root, wort);
		System.out.println("Das Wort " + wort + " gibt es " + anzahl + " mal.");
	}
	private void haeufigkeitIntern(StringNode root,String wort)
	{
		if(root.left != null)
		{
			haeufigkeitIntern(root.left, wort);
		}
		if(root.right != null)
		{
			haeufigkeitIntern(root.right, wort);
		}
		if(root.wort != null)
		{
			if(root.wort.equals(wort))
			{
				anzahl++;
			}
		}
	}
	public void durchschnittHaeufigkeit(StringNode root, String wort)
	{
		anzahl = 0;
		anzahlAll = 0;
		durchschnittHaeufigkeitIntern(root, wort);
		alleWoerter2(root);
		System.out.println("Das Wort " + wort + " gibt es durchschnittlich " + anzahlAll / anzahl + " mal.");
	}
	private void durchschnittHaeufigkeitIntern(StringNode root,String wort)
	{
		anzahlAll++;
		if(root.left != null)
		{
			durchschnittHaeufigkeitIntern(root.left, wort);
		}
		if(root.right != null)
		{
			durchschnittHaeufigkeitIntern(root.right, wort);
		}
		if(root.wort != null)
		{
			if(root.wort.equals(wort))
			{
				anzahl++;
			}
		}
	}
	public void alleWoerter(StringNode root)
	{
		anzahlAll = 0;
		alleWoerterIntern(root);
		System.out.println("Es gibt " + anzahlAll + " Wörter");
	}
	public void alleWoerter2(StringNode root)
	{
		anzahlAll = 0;
		alleWoerterIntern(root);
	}
	private void alleWoerterIntern(StringNode root)
	{
		if(root.wort != null)
		anzahlAll++;
		
		
		if(root.left != null)
		{
			alleWoerterIntern(root.left);
		}
		
		if(root.right != null)
		{
			alleWoerterIntern(root.right);
		}
	}
	
	public void unterschiedlicheWoerter(StringNode root) 
	{
		alleWoerter2(root);
		words = new String[anzahlAll];
		anzahlAll = 0;
		anzahl = 0;
		unterschiedlicheWoerterIntern(root);
		anzahl = anzahlAll;
        Set set = new HashSet(Arrays.asList(words));
		anzahl = anzahl - (anzahl - set.size());
		System.out.println("Es gibt " + anzahl + " unterschiedliche Wörter");
	}
	private void unterschiedlicheWoerterIntern(StringNode root)
	{
		if(root.wort != null)
		{
			words[anzahlAll] = root.wort;
			anzahlAll++;
		}
		if(root.left != null)
		{
			unterschiedlicheWoerterIntern(root.left);
		}
		
		if(root.right != null)
		{
			unterschiedlicheWoerterIntern(root.right);
		}
	}
	/*
	public static void main(String[] args) 
	{
		StringTree tree = new StringTree();
		StringNode sNHallo1 = new StringNode("1");
		StringNode sNHallo2 = new StringNode("1");
		StringNode sNHallo3 = new StringNode("1");
		StringNode sNHallo4 = new StringNode("1");
		StringNode sNTest1 = new StringNode("2");
		StringNode sNTest2 = new StringNode("3");
		StringNode sNTest3 = new StringNode("4");
		StringNode sNTest4 = new StringNode("5");
		StringNode sNTest5 = new StringNode("6");
		
		root = new StringNode("0");
		
		root.left = sNTest1;
		root.right = sNHallo1;
		sNTest1.left = sNTest2;
		sNTest2.left = sNTest3;
		sNTest2.right = sNHallo2;
		sNTest3.left = sNHallo3;
		sNHallo1.right = sNHallo4;
		sNHallo4.left = sNTest4;
		sNHallo4.right = sNTest5;
		
		tree.alleWoerter(root);
		tree.haeufigkeit(root, "1");
		tree.durchschnittHaeufigkeit(root, "1");
		tree.unterschiedlicheWoerter(root);
	}
	*/
	
	
}


