package com.bongladesch.sebidaMaven.other;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Arrays 
{
	private static Logger logger = LogManager.getLogger(Arrays.class);
	public void print(double [] temp)
	{
		for(int i = 0; i < temp.length; i++)
		{
			System.out.print(temp[i] + " ");
		}
		System.out.println();
	}
	public void mirror(double [] werte)
	{
		int n  = werte.length;
		for(int i = 0; i < n/2; i++)
		{
			double temp = werte[i];
			werte[i] = werte[n-1-i];
			werte[n-1-i] = temp;
		}
	}
	public double [] rotate (double [] werte,int k)
	{
		double [] werte2 = new double[werte.length];
		for(int i = 0; i < werte.length; i++)
			werte2[i] = werte[i];
		int n = werte.length;
		for(int i = 0; i < k; i++)
		{
			double temp = werte[n-1];
			for(int j = 0; j < werte.length; j++)
			{
				werte2[j+1] = werte2[j];
			}
			werte2[0] = temp;
		}
		return werte2;
		
	}
	public double [] zip(double [] werte, double [] werte2)
	{
		int n = werte.length;
		int m = werte2.length;
		double[] erg = new double[n+m];
		for(int i = 0, j = 0, k = 0; i < n+m;)
		{
			if(i < n)
				erg[k++] = werte[i++];
			if(j < m)
				erg[k++] = werte2[j++];
		}
		return erg;
		
	}
}
